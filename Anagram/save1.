import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class AnagramSolver {
	private Map<LetterInventory,ArrayList<String>> dictInv;

	public AnagramSolver(List<String> dict){
		dictInv = new HashMap<LetterInventory,ArrayList<String>>();
		for(String i : dict){ //Map this word to its inventory
			LetterInventory inv = new LetterInventory(i);
			ArrayList<String> words = dictInv.get(inv); //words that shares the same Inv
			if(words == null){
				words = new ArrayList<String>();
				words.add(i);
				dictInv.put(inv, words);
			} else {
				words.add(i);
			}
		}	
	}
	
	public List<List<String>> getAnagrams(String word, int max){
		LetterInventory inv = new LetterInventory(word);
		Set<LetterInventory> allInv = dictInv.keySet();
		ArrayList<LetterInventory> invArr = new ArrayList<LetterInventory>(allInv.size());
		for(LetterInventory i : allInv){
			invArr.add(i);
		}
		List<List<LetterInventory>> solutions = new ArrayList<List<LetterInventory>>();
		solveAnagrams(invArr, inv, solutions, new ArrayList<LetterInventory>(), max);
		List<List<String>> result = new ArrayList<List<String>>();
		for(int i = 0; i < solutions.size(); i++){
			List<LetterInventory> anagram = solutions.get(i);
			formGroups(anagram, result, 0, new ArrayList<String>());
		}
		Collections.sort(result, new AnagramComparator());
		return result;
	}
//	
//	private int minimizeScope(List<LetterInventory> invArr, LetterInventory current){
//		for(int i = invArr.size() - 1; i >= 0;i--){
//			invArr.get()
//		}
//	}
//	
	//forming all the combination of words given an ArrayList of LetterInventory
	private void formGroups(List<LetterInventory> anagram, List<List<String>> groups, int index, List<String> group){
		if(index == anagram.size()){
			List<String> groupCopy = deepCopy(group);
			Collections.sort(groupCopy);
			boolean isDup = false;
			for(int i = 0; i < groups.size() && !isDup; i++){
				isDup = groups.get(i).equals(groupCopy);
			}
			if(!isDup){
				groups.add(groupCopy);
			}
		} else {
			ArrayList<String> words = dictInv.get(anagram.get(index));
			for(int i = 0; i < words.size(); i++){
				group.add(words.get(i));
				formGroups(anagram, groups, index + 1, group);
				group.remove(group.size() - 1);
			}
		}
	}

	
	private void solveAnagrams(ArrayList<LetterInventory> invLeft, LetterInventory word, 
			List<List<LetterInventory>> solutions, ArrayList<LetterInventory> anagram, int max){
		if(max == 0 || max != 0 && anagram.size() <= max){
			if(word.size() == 0){
						solutions.add(deepCopy(anagram));
			} else {
				int length = invLeft.size();
				for(int i = 0; i < length; i++){
					LetterInventory ok = invLeft.get(i);
					LetterInventory sub = word.subtract(ok);
					if(sub != null){
						anagram.add(ok);
						solveAnagrams(invLeft, sub, solutions, anagram, max);
						anagram.remove(anagram.size() - 1);
					}
				}
			}
		}
	}
	
	private <E> List<E> deepCopy(List<E> arr){
		List<E> temp = new ArrayList<E>(arr.size());
		for(int i = 0; i < arr.size(); i++){
			temp.add(arr.get(i));
		}
		return temp;
	}
	
   private static class AnagramComparator implements Comparator<List<String>> {
        public int compare(List<String> a1, List<String> a2) {
            if(a1.size() == a2.size()){
            	int res = 0;
            	for(int i = 0; i < a1.size() && res == 0; i++){
            		res = a1.get(i).compareTo(a2.get(i));
            	}
            	return res;
        	}else{
        		return a1.size() < a2.size() ? -1 : 1;
            }
            	
        }
    } // end of AnagramComparator
	
}
