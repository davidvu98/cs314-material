/* CS 314 STUDENTS: FILL IN THIS HEADER AND THEN COPY AND PASTE IT TO YOUR
 * LetterInventory.java AND AnagramSolver.java CLASSES.
 *
 * Student information for assignment:
 *
 *  On my honor, David, this programming assignment is my own work
 *  and I have not provided this code to any other student.
 *
 *  UTEID: dhv254
 *  email address: davidvu98@gmail.com
 *  Section: Aish's Mice
 *  Grader name: Aish
 *  Number of slip days I am using: 0
 */
public class LetterInventory{
	private static final int NUM_LETTERS;
	private static final char FIRST;
	static {
		NUM_LETTERS = 26;
		FIRST = 'a';
	}
	private int size;
	private int[] freq;
	//pre: input cant be null
	public LetterInventory(String input){
		if(input == null){
			throw new IllegalArgumentException();
		}
		input = input.toLowerCase();
		freq = new int[NUM_LETTERS];
		getFreq(input);
	}
	
	//pre: freq must have the size = NUM_LETTERS
	private LetterInventory(int[] freq, int size){
		this.freq = freq;
		this.size = size;
	}
	
	//get the frequency of the input string
	private void getFreq(String input){
		for(int i = 0; i < input.length(); i++){
			char thisChar = input.charAt(i);
			if(isLetter(thisChar)){
				int charIndex = thisChar - FIRST;
				freq[charIndex]++;
				size++;
			}
		}
	}
	
	
	//return the frequency of the given letter
	//pre: the input char has to be an English letter
	public int get(char ch){
		ch = Character.toLowerCase(ch);
		if(!isLetter(ch))
			throw new IllegalArgumentException();
		int charIndex = ch - FIRST;
		return freq[charIndex];
	}
	
	//adding the 2 LetterInventory
	//pre: o is not null
	public LetterInventory add(LetterInventory o){
		final int ADD = 1;
		return doMath(ADD, o);
	}
	
	//subtracting other inventory from this inventory.
	//pre: o is not null and it's subtractable from this
	public LetterInventory subtract(LetterInventory o){
		final int SUBTRACT = -1;
		return doMath(SUBTRACT, o);
	}
	
	private LetterInventory doMath(int mult, LetterInventory o){
		if(o == null)
			throw new IllegalArgumentException();
		int[] newFreq = new int[NUM_LETTERS];
		boolean valid = true;
		for(int i = 0; i < NUM_LETTERS && valid; i++){
			newFreq[i] = this.freq[i] + mult * o.freq[i];
			if(newFreq[i] < 0)
				valid = false;
		}
		int newSize = this.size + mult * o.size;
		return valid ? new LetterInventory(newFreq, newSize) : null;
	}
	
	//return true if there is not letter in the inventory
	public boolean isEmpty(){
		return size == 0;
	}
	
	
	//return the number of letters stored in the inventory
	public int size(){
		return this.size;
	}
	
	//return true if the character is in the English alphabet
	private boolean isLetter(char ch){
		return ch >= 'a' && ch <= 'z';
	}
	
	public String toString(){
		StringBuilder toString = new StringBuilder();
		for(int i = 0; i < freq.length; i++){
			for(int j = 0; j < freq[i];j++){
				char thisChar = (char)(FIRST + i);
				toString.append(thisChar);
			}
		}
		return toString.toString();
	}
	
	@Override
	public boolean equals(Object obj){
		boolean res = obj instanceof LetterInventory;
		if(res){
			LetterInventory other = (LetterInventory) obj; 
			res = other.size == this.size;
			if(res){
				for(int i = 0; i < NUM_LETTERS && res; i++){
					res = freq[i] == other.freq[i];
				}
			}
		}
		return res;
	}
	

	

	
}
