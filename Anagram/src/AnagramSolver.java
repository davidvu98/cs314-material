import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/* CS 314 STUDENTS: FILL IN THIS HEADER AND THEN COPY AND PASTE IT TO YOUR
 * LetterInventory.java AND AnagramSolver.java CLASSES.
 *
 * Student information for assignment:
 *
 *  On my honor, David, this programming assignment is my own work
 *  and I have not provided this code to any other student.
 *
 *  UTEID: dhv254
 *  email address: davidvu98@gmail.com
 *  Section: Aish's Mice
 *  Grader name: Aish
 *  Number of slip days I am using: 0
 */

public class AnagramSolver {
	private Map<String, LetterInventory> dictInv;
	private List<String> words; // words sorted by size in descending order

	public AnagramSolver(List<String> dict) {
		dictInv = new HashMap<String, LetterInventory>();
		for (String i : dict) { // Map this word to its inventory
			LetterInventory inv = new LetterInventory(i);
			dictInv.put(i, inv);
		}
		words = deepCopy(dict); // instead of calling .keySet() every round, we
								// only have to do this once
		Collections.sort(words, new SortBySize()); // sort descending order
	}

	// return a list of anagrams
	public List<List<String>> getAnagrams(String word, int max) {
		LetterInventory current = new LetterInventory(word); // current inv to be filled up
		if (max == 0) { // if there is no limit. then find the max possible limit
			max = getMax(words, current, Integer.MAX_VALUE);
		}
		List<String> wordsLeft = minimizeScope(words, current, max);
		List<List<String>> solutions = new ArrayList<List<String>>();
		if (wordsLeft.size() > 0) {
			max = getMax(wordsLeft, current, max); // get max again after reScoping
			// the reason why i'm creating this array of LetterInventory because
			// I found out that
			// dictInv.get() timing is O(n) for some cases, which makes it a lot
			// slower for long words
			// Arraylist.get() timing is always O(1)
			List<LetterInventory> invLeft = getInventoryArr(wordsLeft);
			solveAnagrams(wordsLeft, invLeft, current, solutions, new ArrayList<String>(), max, 0);
			Collections.sort(solutions, new AnagramComparator()); // sort the anagrams
		}
		return solutions;
	}

	// invLeft is sorted in size order
	// return the the min of biggestMax and input max.
	// the biggest possible limit would be: current.size() / smallestSize
	private int getMax(List<String> wordsLeft, LetterInventory current, int max) {
		if (wordsLeft.size() > 0) {
			int smallestSize = dictInv.get(wordsLeft.get(wordsLeft.size() - 1)).size();
			int biggestMax = current.size() / smallestSize; // setting the most possible max
			return Math.min(biggestMax, max);
		} else {
			return 0;
		}
	}

	// Method to minimize the scope of words
	// max: maximum # of words left to form an anagram
	// current: current LetterInventory that need to be filled by the sum of
	// those possible words
	private List<String> minimizeScope(List<String> wordsLeft, LetterInventory current, int max) {
		List<String> invCopy = new ArrayList<String>(0);
		// wordsLeft is sorted in descending order by inventory size
		int maxSize = wordsLeft.size() == 0 ? 0 : dictInv.get(wordsLeft.get(0)).size();
		if (current.size() <= maxSize * max) { // if it's greater than then not possible to form anagram
			invCopy = new ArrayList<String>(wordsLeft.size());
			for (int i = 0; i < wordsLeft.size(); i++) {
				LetterInventory nextTarget = current.subtract(dictInv.get(wordsLeft.get(i)));
				if (nextTarget != null) {
					invCopy.add(wordsLeft.get(i));
				}
			}
		}
		return invCopy;
	}

	// invLeft is sorted by size descendingly
	// reduce # of HashMap.get() calls because it's sometimes an O(n) operation
	// wordsLeft index corresponds to invLeft index
	private List<LetterInventory> getInventoryArr(List<String> wordsLeft) {
		List<LetterInventory> res = new ArrayList<LetterInventory>(wordsLeft.size());
		for (int i = 0; i < wordsLeft.size(); i++) {
			res.add(dictInv.get(wordsLeft.get(i)));
		}
		return res;
	}

	// return if it's possible to form the target inventory given maximum # of
	// words left
	// invLeft is sorted in descending order by Inventory size
	private boolean isFormable(List<LetterInventory> invLeft, LetterInventory target, int left, int index) {
		int maxSize = invLeft.size() == 0 ? 0 : invLeft.get(index).size(); // the max inventory size
		return target.size() <= maxSize * left;
	}

	// recursive method to solve anagrams
	// List<LetterInventory> invLeft is used to reduce the # of hashMap.get()
	// calls - O(n) sometimes
	// wordsLeft and invLeft indexes are corresponding.
	private void solveAnagrams(List<String> wordsLeft, List<LetterInventory> invLeft, LetterInventory target,
			List<List<String>> solutions, List<String> anagram, int max, int index) {
		if (anagram.size() < max || target.isEmpty()) {
			if (target.isEmpty()) { // base case
				List<String> anaCopy = deepCopy(anagram);
				Collections.sort(anaCopy); // sort alphabetically
				solutions.add(anaCopy);
			} else {
				int length = wordsLeft.size();
				int numLeft = max - anagram.size(); // maximum # of words left to form anagram
				if(isFormable(invLeft, target, numLeft, index)) {
					for (int i = index; i < length; i++) {
						LetterInventory temp = invLeft.get(i);
						LetterInventory newTarget = target.subtract(temp);
						if (newTarget != null) { // still subtractable
							anagram.add(wordsLeft.get(i));
							solveAnagrams(wordsLeft, invLeft, newTarget, solutions, anagram, max, i);
							anagram.remove(anagram.size() - 1); // reverse
						}
					}
				}
			}
		}
	}

	// deepCopy List
	private <E> List<E> deepCopy(List<E> arr) {
		List<E> temp = new ArrayList<E>(arr.size());
		for (int i = 0; i < arr.size(); i++) {
			temp.add(arr.get(i));
		}
		return temp;
	}

	// pre: o1 and o2 aren't null and they have to be in dictInv
	private class SortBySize implements Comparator<String> {
		@Override
		public int compare(String o1, String o2) {
			return dictInv.get(o2).size() - dictInv.get(o1).size(); // sort descendingly
		}

	}

	private static class AnagramComparator implements Comparator<List<String>> {
		public int compare(List<String> a1, List<String> a2) {
			if (a1.size() == a2.size()) { // if size are the same
				int res = 0;
				for (int i = 0; i < a1.size() && res == 0; i++) {
					res = a1.get(i).compareTo(a2.get(i)); // compare the two ArrayList using builtin equals()
				}
				return res;
			} else {
				return a1.size() < a2.size() ? -1 : 1; // sort by size
			}

		}
	} // end of AnagramComparator
}