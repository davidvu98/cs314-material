import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


public class AnagramSolver3 {
	private Map<LetterInventory,ArrayList<String>> dictInv;

	public AnagramSolver3(List<String> dict){
		dictInv = new HashMap<LetterInventory,ArrayList<String>>();
		for(String i : dict){ //Map this word to its inventory
			LetterInventory inv = new LetterInventory(i);
			ArrayList<String> words = dictInv.get(inv); //words that share the same Inv
			if(words == null){
				words = new ArrayList<String>();
				words.add(i);
				dictInv.put(inv, words);
			} else {
				words.add(i);
			}
		}	
	}
	
	//return a list of anagrams
	public List<List<String>> getAnagrams(String word, int max){
		LetterInventory current = new LetterInventory(word); //current inv to be filled up
		Set<LetterInventory> allInv = dictInv.keySet();
		List<LetterInventory> invLeft = new ArrayList<LetterInventory>(allInv);
		Collections.sort(invLeft, Collections.reverseOrder()); //sort them in size. LetterInventory implements Comparable
		if(max == 0){ // if there is no limit. then find the biggest possible limit
			max = getMax(invLeft, current, Integer.MAX_VALUE);
		}
		invLeft = minimizeScope(invLeft, current, max);
		max = getMax(invLeft, current, max); // get max again after reScoping
		List<List<LetterInventory>> solutions = new ArrayList<List<LetterInventory>>();
		solveAnagrams(invLeft, current, solutions, new ArrayList<LetterInventory>(), max, 0);

		List<List<String>> result = new ArrayList<List<String>>();
		for(int i = 0; i < solutions.size(); i++){
			List<LetterInventory> anagram = solutions.get(i);
			formGroups(anagram, result, 0, new ArrayList<String>()); //convert LetterInventory to groups of words
		}
	
		Collections.sort(result, new AnagramComparator()); //sort the anagrams
		return result;
	}
	
	//invLeft is sorted in size order
	//return the the min of biggestMax and input max.
	//the biggest possible limit would be: current.size() / smallestSize
	private int getMax(List<LetterInventory> invLeft, LetterInventory current, int max){
		if(invLeft.size() > 0){
			int smallestSize = invLeft.get(invLeft.size() - 1).size();
			int biggestMax = current.size()  / smallestSize; //setting the most possible max
			return Math.min(biggestMax, max);
		} else {
			return 0;
		}
	}
	
	//Method to minimize the invLeft
	//left: # of words left to form an anagram
	//current: current LetterInventory that need to be filled by the sum of those possible words
	private List<LetterInventory> minimizeScope(List<LetterInventory> invLeft, LetterInventory current, int left){
		List<LetterInventory> invCopy = new ArrayList<LetterInventory>(0);
		if(isFormable(invLeft,current,left, 0)){ //if it's greater than then not possible to form an anagram
			invCopy = new ArrayList<LetterInventory>(invLeft.size());
			for(int i = 0; i < invLeft.size();i++){
				LetterInventory nextTarget = current.subtract(invLeft.get(i));
				if(nextTarget != null){
					invCopy.add(invLeft.get(i));
				}
			}
		}
		return invCopy;
	}
	
	//Recursively forming the word anagrams given the LetterInventory 
	//**Each eventory may contain more than 1 word(s)
	//forming all the combination of words given an ArrayList of LetterInventory
	private void formGroups(List<LetterInventory> anagram, List<List<String>> groups, int index, List<String> group){
		if(index == anagram.size()){
			List<String> groupCopy = deepCopy(group);
			Collections.sort(groupCopy);
			boolean isDup = false;
			for(int i = 0; i < groups.size() && !isDup; i++){ //find duplicates
				isDup = groups.get(i).equals(groupCopy); //ArrayList built in equals
			}
			if(!isDup){
				groups.add(groupCopy);
			}
		} else {
			ArrayList<String> words = dictInv.get(anagram.get(index));
			for(int i = 0; i < words.size(); i++){
				group.add(words.get(i));
				formGroups(anagram, groups, index + 1, group);
				group.remove(group.size() - 1);
			}
		}
	}
	

	
	//return if it's possible to form the target inventory given maximum # of words left
	//and the inventory
	private boolean isFormable(List<LetterInventory> invLeft,  LetterInventory target, int left, int index){
		int maxSize = invLeft.size() == 0 ? 0 : invLeft.get(index).size(); // the max inventory size
		return target.size() <= maxSize * left;
	}
	
	//change the invLeft virtually
	private void solveAnagrams(List<LetterInventory> invLeft, LetterInventory target, 
			List<List<LetterInventory>> solutions, ArrayList<LetterInventory> anagram, int max, int index){
			if(anagram.size() <= max){
				if(target.size() == 0){ //base case
					solutions.add(deepCopy(anagram));
				} else {
					int length = invLeft.size();
					int numLeftToFind = max - anagram.size();
					if(isFormable(invLeft, target, numLeftToFind, index)){ //if it's greater then then not possible to form anagram
						for(int i = index; i < length; i++){
							LetterInventory temp = invLeft.get(i);
							LetterInventory newTarget = target.subtract(temp); 
							if(newTarget != null){ 
								anagram.add(temp);
								solveAnagrams(invLeft, newTarget, solutions, anagram, max, i);
								anagram.remove(anagram.size() - 1);
							}
						}
					}
				}
			}
	}
	
	//deepCopy List
	private <E> List<E> deepCopy(List<E> arr){
		List<E> temp = new ArrayList<E>(arr.size());
		for(int i = 0; i < arr.size(); i++){
			temp.add(arr.get(i));
		}
		return temp;
	}
	
   private static class AnagramComparator implements Comparator<List<String>> {
        public int compare(List<String> a1, List<String> a2) {
            if(a1.size() == a2.size()){ //if size are the same
            	int res = 0;
            	for(int i = 0; i < a1.size() && res == 0; i++){
            		res = a1.get(i).compareTo(a2.get(i)); //compare the two ArrayList using builtin equals()
            	}
            	return res;
        	}else{
        		return a1.size() < a2.size() ? -1 : 1; //sort by size
            }
            	
        }
    } // end of AnagramComparator	
}